package nl.sidnfonds.rioradar.model;

import org.testng.Assert;
import org.testng.annotations.Test;


public class AthleteImplTest {

	@Test(groups = "unit")
	public void testNormalization() {
		Assert.assertEquals(new AthleteImpl("id02", "Kiki Bertens", "Tennis", 231).getNormalizedName(), "Kiki Bertens");
		Assert.assertEquals(new AthleteImpl("id02", "Noël van 't End", "Tennis", 231).getNormalizedName(), "Noel van t End");
		Assert.assertEquals(new AthleteImpl("id02", "Noël Ter-Aar", "Tennis", 231).getNormalizedName(), "Noel Ter Aar");
	}
	
}
