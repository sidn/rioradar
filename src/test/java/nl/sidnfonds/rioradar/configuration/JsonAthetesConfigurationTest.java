package nl.sidnfonds.rioradar.configuration;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.fasterxml.jackson.databind.ObjectMapper;

import static org.mockito.Mockito.*;

public class JsonAthetesConfigurationTest {

	@Test(groups = "unit")
	public void testDeserialization() {
		ConfigurationManager config = mock(ConfigurationManager.class);
		when(config.getProperty(JsonAtheletesConfiguration.FILE_PROPERTY, JsonAtheletesConfiguration.FILE_DEFAULT)).thenReturn("athletes.json");
		ObjectMapper mapper = new ObjectMapper();
		
		JsonAtheletesConfiguration atheletesConfig = new JsonAtheletesConfiguration(config, mapper);
		AthleteConfig athlete = atheletesConfig.get("angela_merkel");
		
		Assert.assertNotNull(athlete);
		Assert.assertEquals(athlete.getName(), "Angela Merkel");
		Assert.assertEquals(athlete.getSport(), "German Politics");
		Assert.assertEquals(athlete.getQuery(), "\"Angela Merkel\" OR face:\"angela_merkel\"");
	}
	
}
