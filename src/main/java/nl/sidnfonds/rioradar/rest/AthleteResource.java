package nl.sidnfonds.rioradar.rest;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import nl.sidnfonds.rioradar.model.Athlete;
import nl.sidnfonds.rioradar.service.AthleteService;

import org.glassfish.hk2.api.Immediate;
import org.jvnet.hk2.annotations.Service;

@Immediate
@Path("/api/1/")
@Singleton @Service
public class AthleteResource {

	private final AthleteService athleteService;
	
	@Inject
	public AthleteResource(AthleteService athleteService) {
		this.athleteService = athleteService;
	}

	@GET
	@Path("/athletes/top")
	@NoCaching
	@Produces(MediaType.APPLICATION_JSON)
	public Response getTopAthletes(@QueryParam("size") Integer size) {
		if (size == null) {
			size = 8;
		}
		List<Athlete> athletes = athleteService.getTopAthletes(size);
		return Response.ok(athletes).build();
	}
	
	@GET
	@Path("/athletes")
	@NoCaching
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAthletes() {
		List<Athlete> athletes = athleteService.getAll();
		return Response.ok(athletes).build();
	}
	
}
