package nl.sidnfonds.rioradar.rest;

import java.util.HashMap;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.glassfish.jersey.server.mvc.Viewable;
import org.jvnet.hk2.annotations.Service;

import nl.sidnfonds.rioradar.model.Athlete;
import nl.sidnfonds.rioradar.service.AthleteService;

@Path("/")
@Singleton @Service
public class HomeResource {

	private final AthleteService athleteService;
	
	@Inject
	public HomeResource(AthleteService athleteService) {
		this.athleteService = athleteService;
	}
	
	@GET
	@Produces(MediaType.TEXT_HTML)
	public Response getView() {
		HashMap<String, String> variables = new HashMap<>();
		return Response.ok(new Viewable("/index.jsp", variables)).build();
	}
	
	@GET
	@Path("athletes/{athlete}")
	@Produces(MediaType.TEXT_HTML)
	public Response getAthleteView(@PathParam("athlete") String athleteId) {
		Athlete athlete = athleteService.getAthlete(athleteId);
		HashMap<String, Object> variables = new HashMap<>();
		variables.put("athlete", athlete);
		variables.put("path", "../");
		return Response.status(Status.NOT_FOUND).entity(new Viewable("/no_longer_found.jsp", variables)).build();
	}
	
}
