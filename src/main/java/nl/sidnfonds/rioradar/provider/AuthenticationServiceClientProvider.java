package nl.sidnfonds.rioradar.provider;

import java.util.concurrent.atomic.AtomicReference;

import javax.inject.Inject;

import org.glassfish.hk2.api.Factory;

import nl.sidnfonds.rioradar.configuration.ConfigurationManager;
import tv.mediadistillery.api.client.authenticationservice.AuthenticationServiceClient;
import tv.mediadistillery.api.client.authenticationservice.Credential;
import tv.mediadistillery.api.client.authenticationservice.HttpAuthenticationServiceClient;
import tv.mediadistillery.api.client.authenticationservice.UserGeneratedCredential;

public class AuthenticationServiceClientProvider implements Factory<AuthenticationServiceClient> {

	private final ConfigurationManager config;
	private final AtomicReference<AuthenticationServiceClient> clientInstance = new AtomicReference<>();
	
	@Inject
	public AuthenticationServiceClientProvider(ConfigurationManager config) {
		this.config = config;
	}

	@Override
	public AuthenticationServiceClient provide() {
		if (clientInstance.get() == null) {
			String username = config.getProperty("authenticationservice.account.username");
			String password = config.getProperty("authenticationservice.account.password");
			String authServiceHost = config.getProperty("authenticationservice.host");
			String clientId = config.getProperty("authenticationservice.clientId");
			String clientSecret = config.getProperty("authenticationservice.clientSecret");
			Credential credential = new UserGeneratedCredential(username, password);
			AuthenticationServiceClient authServiceClient = new HttpAuthenticationServiceClient.Builder()
				.setHost(authServiceHost)
				.setCredential(credential)
				.setClientCredentials(clientId, clientSecret)
				.build();
			clientInstance.set(authServiceClient);
		}
		return clientInstance.get();
	}

	@Override
	public void dispose(AuthenticationServiceClient client) { }
	
}
