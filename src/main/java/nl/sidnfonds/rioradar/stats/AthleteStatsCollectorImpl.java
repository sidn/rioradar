package nl.sidnfonds.rioradar.stats;

import javax.inject.Inject;
import javax.inject.Singleton;

import org.jvnet.hk2.annotations.Service;

import nl.sidnfonds.rioradar.configuration.AthleteConfig;
import nl.sidnfonds.rioradar.configuration.ConfigurationManager;
import nl.sidnfonds.rioradar.model.Athlete;
import nl.sidnfonds.rioradar.model.AthleteImpl;
import tv.mediadistillery.api.client.searchservice.SearchResult;
import tv.mediadistillery.api.client.searchservice.SearchResultProgram;
import tv.mediadistillery.api.client.searchservice.SearchResultSnippet;
import tv.mediadistillery.api.client.searchservice.SearchServiceClient;
import tv.mediadistillery.api.client.searchservice.SearchServiceClient.Sort;

@Service
@Singleton
public class AthleteStatsCollectorImpl implements AthleteStatsCollector {

	private final SearchServiceClient searchClient;
	private final int speechScore;
	private final int faceScore;
	private final int defaultScore;
	
	@Inject
	public AthleteStatsCollectorImpl(SearchServiceClient searchClient, ConfigurationManager config) {
		this.searchClient = searchClient;
		this.speechScore = config.getIntProperty("athletes.score.speech", 15);
		this.faceScore = config.getIntProperty("athletes.score.face", 2);
		this.defaultScore = config.getIntProperty("athletes.score.other", 1);
	}
	
	@Override
	public Athlete getAthleteScore(AthleteConfig config) {
		int totalScore = 0;
		SearchResult result = searchClient.search(config.getQuery(), 0, 10000, Sort.START_DESC, "start > \"01-08-2016\"");
		if(result != null && result.getPrograms() != null) {
			for(SearchResultProgram program : result.getPrograms()) {
				for(SearchResultSnippet snippet : program.getSnippets()) {
					totalScore += scoreMentionsIn(snippet);
				}
			}
		}
		Athlete athlete = new AthleteImpl(config.getId(), config.getName(), config.getSport(), totalScore);
		return athlete;
	}

	private int scoreMentionsIn(SearchResultSnippet snippet) {
		String text = snippet.getSnippet();
		String source = snippet.getSource();
		int numOfMentions = text.split("<em>").length;
		if (source.equalsIgnoreCase("annotations.md::speech")) {
			return speechScore * numOfMentions;
		} else if (source.equalsIgnoreCase("annotations.md::face")) {
			return faceScore * numOfMentions;
		} else {
			return defaultScore * numOfMentions;
		}
	}

}
