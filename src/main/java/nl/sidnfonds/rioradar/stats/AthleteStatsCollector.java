package nl.sidnfonds.rioradar.stats;

import org.jvnet.hk2.annotations.Contract;

import nl.sidnfonds.rioradar.configuration.AthleteConfig;
import nl.sidnfonds.rioradar.model.Athlete;

@Contract
public interface AthleteStatsCollector {

	Athlete getAthleteScore(AthleteConfig config);
	
}
