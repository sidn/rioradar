package nl.sidnfonds.rioradar.service;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import javax.inject.Inject;
import javax.inject.Singleton;

import org.jvnet.hk2.annotations.Service;

import nl.sidnfonds.rioradar.configuration.AthleteConfig;
import nl.sidnfonds.rioradar.configuration.AthletesConfiguration;
import nl.sidnfonds.rioradar.configuration.ConfigurationManager;
import tv.mediadistillery.api.client.contentservice.ContentServiceClient;
import tv.mediadistillery.api.client.contentservice.ContentServiceClient.PlayerType;
import tv.mediadistillery.api.client.contentservice.ContentServiceClient.Protocol;
import tv.mediadistillery.api.client.contentservice.PlayerRequest;
import tv.mediadistillery.api.client.contentservice.PlayerResponse;
import tv.mediadistillery.api.client.contentservice.PlayerUrl;
import tv.mediadistillery.api.client.contentservice.Position;
import tv.mediadistillery.api.client.searchservice.SearchResult;
import tv.mediadistillery.api.client.searchservice.SearchResultProgram;
import tv.mediadistillery.api.client.searchservice.SearchResultSnippet;
import tv.mediadistillery.api.client.searchservice.SearchServiceClient;
import tv.mediadistillery.api.client.searchservice.SearchServiceClient.Sort;

@Service
@Singleton
public class AthleteClipServiceImpl implements AthleteClipService {
	
	private final AthletesConfiguration athletesConfig;
	private final SearchServiceClient searchClient;
	private final ContentServiceClient contentClient;
	private final Integer numOfClips;
	
	@Inject
	public AthleteClipServiceImpl(AthletesConfiguration athletesConfig, SearchServiceClient searchClient, ContentServiceClient contentClient, ConfigurationManager config) {
		this.athletesConfig = athletesConfig;
		this.searchClient = searchClient;
		this.contentClient = contentClient;
		this.numOfClips = config.getIntProperty("athletes.clips", 15);
	}

	@Override
	public List<SearchResultProgram> getAthleteClips(String athleteId) {
		AthleteConfig athleteConfig = athletesConfig.get(athleteId);
		if (athleteConfig != null) {
			SearchResult searchResult = searchClient.search(athleteConfig.getQuery(), 0, numOfClips, Sort.START_DESC, "start > \"01-08-2016\"");
			if (searchResult.getTotalResults() > 0) {
				return searchResult.getPrograms();
			} 
		}
		return new ArrayList<SearchResultProgram>();
	}

	@Override
	public String getAthleteClip(String athleteId, UUID programId, int position) {
		if (isAthleteClip(athleteId, programId, position)) {
			List<Position> positions = new ArrayList<>(1);
			positions.add(new Position(position / 1000));
			PlayerRequest playerRequest = new PlayerRequest(programId, positions);
			List<PlayerRequest> requestedPrograms = new ArrayList<>();
			requestedPrograms.add(playerRequest);
			List<PlayerResponse> response = contentClient.getBulkPlayers(requestedPrograms, PlayerType.PRIVATE, Protocol.HTTPS);
			if (response.size() == 1 && response.get(0).getUrls().values().size() == 1) {
				return response.get(0).getUrls().values().toArray(new PlayerUrl[0])[0].getUrl();
			} else {
				return null;
			}			
		} else {
			return null;
		}
	}
	
	public boolean isAthleteClip(String athleteId, UUID programId, int position) {
		AthleteConfig athleteConfig = athletesConfig.get(athleteId);
		if (athleteConfig != null) {
			String query = "id:" + programId.toString() + " AND (" + athleteConfig.getQuery() + ")";
			SearchResult searchResult = searchClient.search(query, 0, 1, Sort.START_DESC);
			if (searchResult.getSize() == 1) {
				List<SearchResultSnippet> snippets = searchResult.getPrograms().get(0).getSnippets();
				for (SearchResultSnippet snippet: snippets) {
					if ((snippet.getStart() - 5000) < position && (snippet.getStop() + 5000) > position) {
						return true;
					}
				}
			}
		}
		return false;
	}
	
}
