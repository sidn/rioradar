package nl.sidnfonds.rioradar.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;

import javax.inject.Inject;
import javax.inject.Singleton;

import nl.sidnfonds.rioradar.configuration.AthleteConfig;
import nl.sidnfonds.rioradar.configuration.AthletesConfiguration;
import nl.sidnfonds.rioradar.model.Athlete;
import nl.sidnfonds.rioradar.model.AthleteDescendingScoreComparator;
import nl.sidnfonds.rioradar.model.AthleteNameComparator;
import nl.sidnfonds.rioradar.stats.AthleteStatsCollector;

import org.apache.log4j.Logger;
import org.jvnet.hk2.annotations.Service;

@Service
@Singleton
public class CachedAthleteService implements AthleteService {

	private final Logger LOG = Logger.getLogger(CachedAthleteService.class);
	
	//Keeps all Athletes sorted in the map.
	private final AtomicReference<Map<String,Athlete>> athletes = new AtomicReference<Map<String,Athlete>>();
	private final AthletesConfiguration athletesConfig;
	private final AthleteStatsCollector athletesStatsCollector;
	private final ScheduledExecutorService executor = Executors.newScheduledThreadPool(1);
	
	@Inject
	public CachedAthleteService(AthletesConfiguration athletesConfig, AthleteStatsCollector collector) {
		this.athletesConfig =  athletesConfig;
		this.athletesStatsCollector = collector;
		executor.scheduleAtFixedRate(new RefreshRunner(), 0, 15, TimeUnit.MINUTES);
	}
	
	@Override
	public List<Athlete> getTopAthletes(int count) {
		if(requiresRefresh()) {
			refreshAthleteStats();
		}
		List<Athlete> result = new ArrayList<Athlete>(count);
		Iterator<Athlete> iter = athletes.get().values().iterator();
		for(int i=0;i<count;i++) {
			if(iter.hasNext()) {
				result.add(iter.next());
			} else {
				break;
			}
		}
		return result;
	}

	@Override
	public Athlete getAthlete(String id) {
		if(requiresRefresh()) {
			refreshAthleteStats();
		}
		return athletes.get().get(id);
	}

	private void refreshAthleteStats() {
		LOG.info("Reloading statistics for all Athletes...");
		List<Athlete> allAthletes = new LinkedList<Athlete>();
		List<AthleteConfig> configuredAthletes = athletesConfig.getAll();
		for(AthleteConfig config : configuredAthletes) {
			Athlete athlete = null;
			try {
				athlete = athletesStatsCollector.getAthleteScore(config);
				allAthletes.add(athlete);
				LOG.info("Athlete "+athlete.getName()+": "+athlete.getScore());
			} catch(Exception e) {
				LOG.error("Unable to load data for athlete "+athlete.getName(), e);
			}
		}
		
		Collections.sort(allAthletes, new AthleteDescendingScoreComparator());
		Map<String,Athlete> map = new LinkedHashMap<String,Athlete>();
		for(Athlete a : allAthletes) {
			map.put(a.getId(), a);
			LOG.info("Name: "+a.getName()+" score: "+a.getScore());
		}
		athletes.set(map);
		LOG.info("Pulbished new statistic for all Athletes.");
	}


	private boolean requiresRefresh() {
		return athletes.get() == null;
	}

	private class RefreshRunner implements Runnable {

		@Override
		public void run() {
			try {
			refreshAthleteStats();
			} catch(Exception e) {
				LOG.error("Unhandled exception while reloading data for athletes.");
			}
		}
		
	}

	@Override
	public List<Athlete> getAll() {
		List<Athlete> all = new LinkedList<Athlete>(athletes.get().values());
		Collections.sort(all, new AthleteNameComparator());
		return all;
	}
	
}

	