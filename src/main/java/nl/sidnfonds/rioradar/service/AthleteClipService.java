package nl.sidnfonds.rioradar.service;

import java.util.List;
import java.util.UUID;

import org.jvnet.hk2.annotations.Contract;

import tv.mediadistillery.api.client.searchservice.SearchResultProgram;

@Contract
public interface AthleteClipService {

	public List<SearchResultProgram> getAthleteClips(String athleteId); 
	
	public String getAthleteClip(String athleteId, UUID programId, int position);
	
}
