package nl.sidnfonds.rioradar.service;

import java.util.List;

import org.jvnet.hk2.annotations.Contract;

import nl.sidnfonds.rioradar.model.Athlete;

@Contract
public interface AthleteService {

	public List<Athlete> getTopAthletes(int count);

	public Athlete getAthlete(String id);

	public List<Athlete> getAll();
	
}
