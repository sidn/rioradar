package nl.sidnfonds.rioradar.web;

import nl.sidnfonds.rioradar.configuration.AthletesConfiguration;
import nl.sidnfonds.rioradar.configuration.ConfigurationManager;
import nl.sidnfonds.rioradar.configuration.JsonAtheletesConfiguration;
import nl.sidnfonds.rioradar.configuration.YamlConfigurationManager;
import nl.sidnfonds.rioradar.provider.AuthenticationServiceClientProvider;
import nl.sidnfonds.rioradar.provider.ContentServiceClientProvider;
import nl.sidnfonds.rioradar.provider.SearchServiceClientProvider;
import nl.sidnfonds.rioradar.service.AthleteClipService;
import nl.sidnfonds.rioradar.service.AthleteClipServiceImpl;
import nl.sidnfonds.rioradar.service.AthleteService;
import nl.sidnfonds.rioradar.service.CachedAthleteService;
import nl.sidnfonds.rioradar.stats.AthleteStatsCollector;
import nl.sidnfonds.rioradar.stats.AthleteStatsCollectorImpl;

import javax.inject.Singleton;

import org.apache.commons.lang3.ArrayUtils;
import org.glassfish.hk2.utilities.BuilderHelper;
import org.glassfish.hk2.utilities.binding.AbstractBinder;

import tv.mediadistillery.api.client.authenticationservice.AuthenticationServiceClient;
import tv.mediadistillery.api.client.contentservice.ContentServiceClient;
import tv.mediadistillery.api.client.searchservice.SearchServiceClient;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;

public class ApplicationBinder extends AbstractBinder {
	
	private static final String[] CONFIG_FILES = ArrayUtils.toArray("/etc/md-rioradar/rioradar-config.yaml",
			"./src/main/resources/rioradar-config.yaml",
			"./rioradar-config.yaml",
			"rioradar-config.yaml");
	
	@Override
	protected void configure() {
		try {
			// Configuration
			ConfigurationManager config = new YamlConfigurationManager(CONFIG_FILES);
			bind(BuilderHelper.createConstantDescriptor(config, null, ConfigurationManager.class));
			
			// Jackson
			ObjectMapper jackson = new ObjectMapper();
			jackson.setPropertyNamingStrategy(PropertyNamingStrategy.CAMEL_CASE_TO_LOWER_CASE_WITH_UNDERSCORES);
			bind(BuilderHelper.createConstantDescriptor(jackson, null, ObjectMapper.class));
            
			// Providers
			bindFactory(AuthenticationServiceClientProvider.class).to(AuthenticationServiceClient.class);
			bindFactory(SearchServiceClientProvider.class).to(SearchServiceClient.class);
			bindFactory(ContentServiceClientProvider.class).to(ContentServiceClient.class);
			
            // Bind services
			bind(AthleteStatsCollectorImpl.class).to(AthleteStatsCollector.class);
			bindAsContract(CachedAthleteService.class).to(AthleteService.class).in(Singleton.class);
			bindAsContract(AthleteClipServiceImpl.class).to(AthleteClipService.class).in(Singleton.class);
			bind(JsonAtheletesConfiguration.class).to(AthletesConfiguration.class);
			
		} catch (Exception e) {
			throw new RuntimeException("Unable to initialize Dashboard", e);
		}
		
	}

}
