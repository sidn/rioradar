package nl.sidnfonds.rioradar.web;

import javax.inject.Inject;

import org.glassfish.hk2.api.ServiceLocator;
import org.glassfish.hk2.utilities.ServiceLocatorUtilities;
import org.glassfish.jersey.jackson.JacksonFeature;
import org.glassfish.jersey.server.ResourceConfig;

final class RioRadarApplication extends ResourceConfig {

	@Inject
	public RioRadarApplication(ServiceLocator serviceLocator) {
		// Register application binder
		ServiceLocatorUtilities.enableImmediateScope(serviceLocator); 
		register(new ApplicationBinder());
		// Register Jackson
		register(JacksonFeature.class);
		// Configure Jersey
		packages(true, "nl.sidnfonds.rioradar.rest");
		packages(true, "nl.sidnfonds.rioradar.service");
		property("com.sun.jersey.config.property.WebPageContentRegex", "/(img|js|css|font|data)/.*"); // Old configuration style
		property("jersey.config.servlet.filter.staticContentRegex", "/(img|js|css|font|data)/.*");
		property("jersey.config.server.mvc.templateBasePath.jsp", "/views");
		property("jersey.config.server.provider.classnames", "org.glassfish.jersey.server.mvc.jsp.JspMvcFeature");
	}

}
