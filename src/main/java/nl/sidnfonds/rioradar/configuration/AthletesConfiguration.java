package nl.sidnfonds.rioradar.configuration;

import java.util.List;

import org.jvnet.hk2.annotations.Contract;

@Contract
public interface AthletesConfiguration {

	List<AthleteConfig> getAll();
	
	AthleteConfig get(String id);
	
}
