package nl.sidnfonds.rioradar.configuration;


public interface AthleteConfig {

	String getId();
	
	String getName();

	String getSport();
	
	String getQuery();

}
