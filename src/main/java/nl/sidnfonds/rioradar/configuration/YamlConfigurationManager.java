/*******************************************************************************
 * This file is part of the Media Distillery platform.
 * Copyright Media Distillery B.V.
 ******************************************************************************/
package nl.sidnfonds.rioradar.configuration;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;

import javax.inject.Singleton;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.log4j.Logger;
import org.jvnet.hk2.annotations.Service;
import org.yaml.snakeyaml.Yaml;

@Service
@Singleton
public class YamlConfigurationManager implements ConfigurationManager {

	private static final Logger LOG = Logger.getLogger(YamlConfigurationManager.class);
	private AtomicReference<Map<String,Object>> values = new AtomicReference<Map<String,Object>>(new HashMap<String, Object>());
	
	public YamlConfigurationManager(String ... files) throws FileNotFoundException {
		boolean found = false;
		for(String file : files) {
			InputStream is = this.getClass().getClassLoader().getResourceAsStream(file);
			if (is == null) {
				File theFile = new File(file);
				if (theFile.exists()) {
					is = new FileInputStream(theFile);
				}
			}
			if(is != null) {
				try { 
					Reader reader = new InputStreamReader(is, "UTF-8");
					Yaml yaml = new Yaml();
					@SuppressWarnings("unchecked")
					Map<String,Object> loadedValues = (Map<String, Object>) yaml.load(reader);
					if(loadedValues != null) {
						values.set(loadedValues);
					}
					found = true;
					LOG.info("Loading configuration file: "+file);
					break;
				} catch (UnsupportedEncodingException e) {
					throw new IllegalStateException("Unable to load config file. Unsupported encoding.");
				} finally {
					try {
						is.close();
					} catch (IOException e) {
					}
				}
			}
		}
		if (!found) {
			throw new FileNotFoundException("None of the configured config files could be found.");
		}
	}

	public YamlConfigurationManager(String file) throws FileNotFoundException {
		this(ArrayUtils.toArray(file));
	}

	public String getProperty(String property) {
		return getProperty(property, null);
	}

	public String getProperty(String property, String defaultValue) {
		Object value = values.get().get(property);
		if (value == null) {
			return defaultValue;
		}
		return String.valueOf(value);
	}

	public List<String> getPropertyAsList(String property) {
		return getPropertyAsList(property, null);
	}

	@SuppressWarnings("unchecked")
	public List<String> getPropertyAsList(String property, List<String> defaultValues) {
		Object value = values.get().get(property);
		if (value == null) {
			return defaultValues;
		}
		return (List<String>) value;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Object> getPropertyAsObjectList(String property) {
		Object value = values.get().get(property);
		return (List<Object>) value;
	}



	@Override
	public Integer getIntProperty(String property) {
		String val = getProperty(property);
		if(val == null) {
			return null;
		}
		return Integer.valueOf(val);
	}

	@Override
	public Integer getIntProperty(String property, int defaultValue) {
		String val = getProperty(property);
		if(val == null) {
			return defaultValue;
		}
		return Integer.valueOf(val);
	}

	@Override
	public Long getLongProperty(String property) {
		String val = getProperty(property);
		if(val != null) {
			return Long.valueOf(val);
		} else {
			return null;
		}
	}

	@Override
	public Long getLongProperty(String property, long defaultValue) {
		String val = getProperty(property);
		if(val == null) {
			return defaultValue;
		}
		return Long.valueOf(val);
	}
	
	@Override
	public Double getDoubleProperty(String property) {
		String val = getProperty(property);
		if (val != null) {
			return Double.valueOf(val);
		} else {
			return null;
		}
	}

	@Override
	public Double getDoubleProperty(String property, double defaultValue) {
		String val = getProperty(property);
		if (val == null) {
			return defaultValue;
		}
		return Double.valueOf(val);
	}

	@Override
	public Boolean getBooleanProperty(String property) {
		String val = getProperty(property);
		return Boolean.valueOf(val);
	}

	@Override
	public Boolean getBooleanProperty(String property, boolean defaultValue) {
		String val = getProperty(property);
		if(val == null) {
			return defaultValue;
		}
		return Boolean.valueOf(val);
	}

	@SuppressWarnings("unchecked")
	@Override
	public Map<String, String> getPropertyAsMap(String property) {
		Object val = values.get().get(property);
		if(val != null && val instanceof Map) {
			return (Map<String, String>) val;
		}
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Map<String, Object> getPropertyAsObjectMap(String property) {
		Object val = values.get().get(property);
		if(val != null && val instanceof Map) {
			return (Map<String, Object>) val;
		}
		return null;
	}

	@Override
	public boolean hasProperty(String property) {
		return values.get().containsKey(property);
	}
	
}
