package nl.sidnfonds.rioradar.configuration;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.inject.Singleton;

import org.apache.log4j.Logger;
import org.jvnet.hk2.annotations.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Charsets;

@Service
@Singleton
public class JsonAtheletesConfiguration implements AthletesConfiguration {

	static final String FILE_DEFAULT = "./src/main/resources/athletes.json";
	static final String FILE_PROPERTY = "athletes.json";
	private final Logger LOG = Logger.getLogger(JsonAtheletesConfiguration.class);
	private final List<AthleteConfig> athletes = new LinkedList<>();
	private final Map<String,AthleteConfig> index = new HashMap<>();
	
	@Inject
	public JsonAtheletesConfiguration(ConfigurationManager config, ObjectMapper mapper) {
		String filename = config.getProperty(FILE_PROPERTY, FILE_DEFAULT);
		try {
			InputStream in = JsonAtheletesConfiguration.class.getClassLoader().getResourceAsStream(filename);
			InputStreamReader reader = new InputStreamReader(in, Charsets.UTF_8);
			List<AthleteConfig> athletes = mapper.readValue(reader, new TypeReference<LinkedList<AthleteConfigImpl>>(){});
			in.close();
			this.athletes.addAll(athletes);
			for(AthleteConfig c : athletes) {
				index.put(c.getId(), c);
			}
		} catch (IOException e) {
			LOG.error("Unable to load the list of Athletes from "+filename, e);
		}
	}
	
	@Override
	public List<AthleteConfig> getAll() {
		return athletes;
	}

	@Override
	public AthleteConfig get(String id) {
		return index.get(id);
	}

	
	
}

