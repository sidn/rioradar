package nl.sidnfonds.rioradar.configuration;

import java.util.List;
import java.util.Map;

import org.jvnet.hk2.annotations.Contract;

@Contract
public interface ConfigurationManager {

	Integer getIntProperty(String property);

	Integer getIntProperty(String property, int defaultValue);
	
	Long getLongProperty(String property);
	
	Long getLongProperty(String property, long defaultValue);

	Double getDoubleProperty(String property);
	
	Double getDoubleProperty(String property, double defaultValue);
	
	Boolean getBooleanProperty(String property);
	
	Boolean getBooleanProperty(String property, boolean defaultValue);
	
	String getProperty(String property);

	String getProperty(String property, String defaultValue);
	
	List<String> getPropertyAsList(String property);

	List<Object> getPropertyAsObjectList(String property);
	
	List<String> getPropertyAsList(String property, List<String> defaultValues);

	Map<String,String> getPropertyAsMap(String property);

	Map<String,Object> getPropertyAsObjectMap(String property);
	
	boolean hasProperty(String property);
	
}
