package nl.sidnfonds.rioradar.configuration;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown=true)
public class AthleteConfigImpl implements AthleteConfig {

	private final String id;
	private final String fullname;
	private final String query; 
	private final String sport;
	
	@JsonCreator
	public AthleteConfigImpl(@JsonProperty("id") String id, @JsonProperty("name") String fullname, @JsonProperty("sport") String sport, @JsonProperty("query") String query) {
		super();
		this.id = id;
		this.fullname = fullname;
		this.sport = sport;
		this.query = query;
	}

	@Override
	public String getId() {
		return id;
	}

	@Override
	public String getName() {
		return fullname;
	}

	@Override
	public String getQuery() {
		return query;
	}

	@Override
	public String getSport() {
		return sport;
	}
	

}
