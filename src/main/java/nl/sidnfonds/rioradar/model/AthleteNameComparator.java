package nl.sidnfonds.rioradar.model;

import java.util.Comparator;

public class AthleteNameComparator implements Comparator<Athlete> {

	@Override
	public int compare(Athlete o1, Athlete o2) {
		return o1.getName().compareTo(o2.getName());
	}

}
