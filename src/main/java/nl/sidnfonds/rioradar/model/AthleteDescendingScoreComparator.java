package nl.sidnfonds.rioradar.model;

import java.util.Comparator;

public class AthleteDescendingScoreComparator implements Comparator<Athlete> {

	@Override
	public int compare(Athlete o1, Athlete o2) {
		Integer score1 = o1.getScore();
		Integer score2= o2.getScore();
		return score2.compareTo(score1);
	}

}
