package nl.sidnfonds.rioradar.model;


public interface Athlete {

	String getId();
	
	String getName();

	String getNormalizedName();
	
	int getScore();

	String getSport();
	
}
