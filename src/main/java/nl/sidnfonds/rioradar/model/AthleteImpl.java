package nl.sidnfonds.rioradar.model;

import org.apache.commons.lang3.StringUtils;

public class AthleteImpl implements Athlete {

	private final String id;
	private final String fullname;
	private final int score;
	private final String sport;
	
	public AthleteImpl(String id, String fullname, String sport, int score) {
		this.id = id;
		this.fullname = fullname;
		this.sport = sport;
		this.score = score;
	}

	@Override
	public String getId() {
		return id;
	}

	@Override
	public String getName() {
		return fullname;
	}

	@Override
	public int getScore() {
		return score;
	}

	@Override
	public String getSport() {
		return sport;
	}

	@Override
	public String getNormalizedName() {
		return StringUtils.stripAccents(fullname).replace("'", "").replace("’", "").replace("-", " ");
	}
	
}
