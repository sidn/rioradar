<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="author" content="Media Distillery">
		<meta name="description" content="RioRadar volgt onze olympische sporters in beeld tijdens de spelen in Rio. Met geavanceerde beeld en audioherkenning houden we voor jou in de gaten waar jouw favoriete sporters te zien zijn. Mis geen enkel fragment meer! RioRadar is een initiatief van Media Distillery, SpraakLab, TNO en VicarVision en wordt ondersteund door het SIDN Fonds">
		<meta name="keywords" content="Rio, Olympische Spelen, Rio 2016, Olympische Spelen 2016, VicarVision, TNO, SpraakLab, Media Distillery ">
		<meta content="RioRadar" property="og:site_name">
		<meta content="nl_NL" property="og:locale">
		
		<link type="image/x-icon" href="http://www.rioradar.nl/favicon.ico" rel="shortcut icon">
		<link href="img/radar.ico" rel="icon">
		
		<title>RioRadar - Volg onze Olympische sporters!</title>

		<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
		<link rel="stylesheet" type="text/css" href="css/fonts.css">
		<link rel="stylesheet" type="text/css" href="css/base.css">
		
		<!-- [if IE]>
		<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->

		<!-- Google Analytics -->
		<script>
		  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
		  ga('create', 'UA-61645655-3', 'auto');
		  ga('send', 'pageview');
		</script>
		
	</head>

	<body>
	
		<nav class="navbar navbar-inverse navbar-fixed-top">
			<div class="container">
				<div class="brand">
					<a href="/">RioRadar <span class="logo-font"></span></a>
				</div>
				<div class="flag">
					<div class="flag-red"></div>
					<div class="flag-blue"></div>
				</div>
			</div>
		</nav>
	
		<div class="plane-1">
			<div class="container">
				<div class="plane-header">
					<h2>Populairste sporters</h2>
				</div>
				<div class="row">
					<div class="col-xs-12">
						<p>Deze sporters zijn het meest in online video's besproken of te zien geweest:</p>
					</div>
				</div>
				<div id="popular-athletes-loading" class="row">
					<div class="col-xs-12">
						<p>De sporters worden ingeladen</p>
					</div>
				</div>
				<div id="popular-athletes-row" class="row">
					<div class="col-xs-10 col-xs-offset-1">
						<div class="row">
							<!-- Names -->
							<div id="popular-athlete-names" class="sporter-names">
								<!-- Placeholder for names -->
							</div>
							<!-- Photos -->
							<div id="popular-athlete-tiles" class="sporter-tiles">
								<!-- Placeholder for photos -->
							</div>
							<!-- Bars -->
							<div id="popular-athlete-bars" class="sporter-bars">
								<!-- Placeholder for bars -->
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	
		<div class="plane-2">
			<div class="container">
				<div class="row">
					<div class="col-xs-10 col-xs-offset-1">
						<div class="plane-2-text">
							<p>RioRadar volgt voor jou alle Nederlandse sporters in Rio! We houden online video's voor je in de gaten met automatische gezichts- en spraakherkenning. We monitoren alle Nederlandse Olympi&euml;rs en kunnen je precies vertellen hoeveel 'air-time' ze in deze media hebben gehad. Alle geplaatste video’s worden direct geanalyseerd en resultaten worden in realtime verwerkt.</p>
						</div>
					</div>	
				</div>
			</div>
		</div>
	
		<div class="plane-3">
			<div class="container">
				<div class="plane-header">
					<h2>Zoek sporter</h2>
				</div>
				<div class="row">
					<div class="col-xs-12">
						<p>Zoek hieronder je favoriete olympmische sporter.</p>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12">
						<div class="sporter-search">
							<form>
								<div class="col-sm-10 col-xs-offset-1">
									<input type="text" class="form-control input-lg" id="sporter-search-field">
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	
		<div class="plane-4">
			<div class="container">
				<div class="plane-header">
					<h2>Alle sporters</h2>
				</div>
				<div id="browse-athletes-loading" class="row">
					<div class="col-xs-12 ">
						<p>De sporters worden ingeladen</p>
					</div>
				</div>
				<div class="row browse-athletes-row">
					<div class="col-xs-10 col-xs-offset-1">
						<div id="sporter-tabs-nav" class="a-z-nav">
							<!-- Placeholder for navigation -->
						</div>
					</div>
				</div>
				<div class="row browse-athletes-row">
					<div class="col-xs-12 col-sm-12 col-md-12">
						<div id="sporter-tabs" class="tab-content">
							<!-- Placeholder for tabs -->
						</div>
					</div>
				</div>
			</div>
		</div>	

		<div class="plane-5">
			<div class="container">
				<div class="row">
					<div class="logo-col col-sm-8">
						<div class="row">
							<div class="col-sm-12">
								<span>RioRadar is een initiatief van:</span>
							</div>
						</div>
						<div class="row">
							<div class="logo-box col-sm-3">
								<a href="http://www.mediadistillery.tv" target="_blank"><img src="img/mediadistillery_sm.jpg" alt="Media Distillery"/></a>
							</div>
							<div class="logo-box col-sm-3">
								<a href="http://www.spraaklab.nl" target="_blank"><img src="img/spraaklab_sm.jpg" alt="SpraakLab"/></a>
							</div>
							<div class="logo-box col-sm-3">
								<a href="http://www.tno.nl" target="_blank"><img src="img/tno_sm.jpg" alt="TNO"/></a>
							</div>
							<div class="logo-box col-sm-3">
								<a href="http://www.vicarvision.nl" target="_blank"><img src="img/vicarvision_sm.jpg" alt="VicarVision"/></a>
							</div>
						</div>
					</div>
					<div class="col-sm-1"></div>
					<div class="logo-col col-sm-3">
						<div class="row">
							<div class="col-sm-12">
								<span>Met support van:</span>
							</div>
						</div>
						<div class="row">
							<div class="logo-box col-sm-12">
								<a href="https://www.sidnfonds.nl/projecten/media-distillery-online" target="_blank"><img src="img/sidnfonds.jpg" alt="SIDN Fonds"/></a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		
		
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
		
		<!-- TODO: Create JQuery UI with autocomplete only -->
		<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
		
		<script src="js/rioradar.js"></script>
		
		<script>
			$( function() {
				loadPopularAthletes();
				loadAthletes();
			});
		</script>
		
	</body>

</html>