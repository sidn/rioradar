<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:choose>
	<c:when test="${not empty model.path}" >
		<c:set var="path" value="${model.path}"/>
	</c:when>
	<c:otherwise>
		<c:set var="path" value=""/>
	</c:otherwise>
</c:choose>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="author" content="Media Distillery">
		<meta name="description" content="RioRadar volgt onze olympische sporters in beeld tijdens de spelen in Rio. Met geavanceerde beeld en audioherkenning houden we voor jou in de gaten waar jouw favoriete sporters te zien zijn. Mis geen enkel fragment meer! RioRadar is een initiatief van Media Distillery, SpraakLab, TNO en VicarVision en wordt ondersteund door het SIDN Fonds">
		<meta name="keywords" content="${model.athlete.name}, ${model.athlete.sport}, Rio, Olympische Spelen, Rio 2016, Olympische Spelen 2016, VicarVision, TNO, SpraakLab, Media Distillery ">
		<meta content="RioRadar" property="og:site_name">
		<meta content="nl_NL" property="og:locale">
		
		<link type="image/x-icon" href="http://www.rioradar.nl/favicon.ico" rel="shortcut icon">
		<link href="${path}img/radar.ico" rel="icon">
		
		<title>RioRadar - ${model.athlete.name}</title>

		<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
		<link rel="stylesheet" type="text/css" href="${path}css/fonts.css">
		<link rel="stylesheet" type="text/css" href="${path}css/base.css">
		
		<!-- [if IE]>
		<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->

		<!-- Google Analytics -->
		<script>
		  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
		  ga('create', 'UA-61645655-3', 'auto');
		  ga('send', 'pageview');
		</script>
		
		<script type="text/javascript" data-cfasync="false" src="//dsms0mj1bbhn4.cloudfront.net/assets/pub/shareaholic.js" data-shr-siteid="10fcc699dcb0a814465f207c1b16fbb1" async="async"></script>
		
	</head>

	<body>
	
		<nav class="navbar navbar-inverse navbar-fixed-top">
			<div class="container">
				<div class="brand">
					<a href="/">RioRadar <span class="logo-font"></span></a>
				</div>
				<div class="flag">
					<div class="flag-red"></div>
					<div class="flag-blue"></div>
				</div>
			</div>
		</nav>
	
		<div class="plane-6">
			<div class="container">
				<div class="plane-header">
					<h2><c:out value="${model.athlete.name}"/></h2>
				</div>
				<div id="athlete-clips-loading" class="row no-margin">
					<p>Loading clips</p>
				</div>
				<div id="athlete-clips-row" class="row no-margin">
					<p>We hebben deze fragmenten voor ${model.athlete.name} gevonden op basis van spraak- en gezichtsherkenning. Je kunt de video's op NOS.nl bekijken.</p>
				</div>
			</div>
		</div>

		<div class="plane-5">
			<div class="container">
				<div class="row">
					<div class="logo-col col-sm-8">
						<div class="row">
							<div class="col-sm-12">
								<span>RioRadar is een initiatief van:</span>
							</div>
						</div>
						<div class="row">
							<div class="logo-box col-sm-3">
								<a href="http://www.mediadistillery.tv" target="_blank"><img src="${path}img/mediadistillery_sm.jpg" alt="Media Distillery"/></a>
							</div>
							<div class="logo-box col-sm-3">
								<a href="http://www.spraaklab.nl" target="_blank"><img src="${path}img/spraaklab_sm.jpg" alt="SpraakLab"/></a>
							</div>
							<div class="logo-box col-sm-3">
								<a href="http://www.tno.nl" target="_blank"><img src="${path}img/tno_sm.jpg" alt="TNO"/></a>
							</div>
							<div class="logo-box col-sm-3">
								<a href="http://www.vicarvision.nl" target="_blank"><img src="${path}img/vicarvision_sm.jpg" alt="VicarVision"/></a>
							</div>
						</div>
					</div>
					<div class="col-sm-1"></div>
					<div class="logo-col col-sm-3">
						<div class="row">
							<div class="col-sm-12">
								<span>Met support van:</span>
							</div>
						</div>
						<div class="row">
							<div class="logo-box col-sm-12">
								<a href="https://www.sidnfonds.nl/projecten/media-distillery-online" target="_blank"><img src="${path}img/sidnfonds.jpg" alt="SIDN Fonds"/></a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/mustache.js/2.2.1/mustache.min.js"></script>
		
		<script src="${path}js/date.f-0.5.0.js"></script>
		<script src="${path}js/rioradar.js"></script>
		
		<script>
			$( function() {
				var athleteId = "${model.athlete.id}";
				var athleteName = "${model.athlete.name}";
				loadAthleteClips( {
					id: athleteId,
					name: athleteName
				} );
			});
		</script>
		
	</body>

</html>