/*
 * Load the data
 */
function loadPopularAthletes() {
	var popularAthletesUrl = "api/1/athletes/top?size=8";
	//var popularAthletesUrl = "data/athletes.json";
	// Show loading
	$( "#popular-athletes-loading" ).show();
	$( "#popular-athletes-row" ).hide();
	$.ajax({
		type: 'GET',
		url: popularAthletesUrl,
		dataType: "json",
		success: function( data ) {
			$( "#popular-athletes-loading" ).hide();
			$( "#popular-athletes-row" ).show();
			showPopularAthletes( data );
		}
	});
}

function loadAthletes() {
	var athletesUrl = "api/1/athletes";
	//var athletesUrl = "data/athletes.json";
	// Show loading
	$( "#browse-athletes-loading" ).show();
	$( ".browse-athletes-row" ).hide();
	$.ajax({
		type: 'GET',
		url: athletesUrl,
		dataType: "json",
		success: function( data ) {
			$( "#browse-athletes-loading" ).hide();
			$( ".browse-athletes-row" ).show();
			initSuggester( data );
			showAthletes( data );
		}
	});
}


/*
 * Show the popular athletes
 */
function showPopularAthletes( athletes ) {
	var maxScore = 0;
	var minScore = Number.MAX_SAFE_INTEGER;
	for ( var i = 0; i < Math.min( 8, athletes.length ); i++ ) {
		var athlete = athletes[i];
		maxScore = Math.max( maxScore, athlete.score );
		minScore = Math.min( minScore, athlete.score );
	}
	var lowerBoundary = ( maxScore - minScore ) * 0.1;
	for ( var i = 0; i < Math.min( 8, athletes.length ); i++ ) {
		var athlete = athletes[i];
		// Name
		$( "#popular-athlete-names" ).append(
			"<div class=\"sporter-name\">" +
				"<span>" + athlete.name + "</span>" +
			"</div>"		
		);
		// Photo
		$( "#popular-athlete-tiles" ).append(
			"<div class=\"sporter-tile-70\"><img src=\"./img/faces/"+athlete.id+".jpg\"></div>"
		);
		// Bar
		var relativeWidth = Math.round( ( ( athlete.score - minScore + lowerBoundary ) / ( maxScore - minScore + lowerBoundary) ) * 100 );
		var quantity = getQuantity( athlete.score );
		var bar = $( 
			"<div class=\"sporter-bar\">" +
				"<div class=\"duration\">" + quantity.value + "</div>" + 
				"<div class=\"duration-unit\">" + quantity.unit + "</div>" +
			"</div>"
		).data( "score", athlete.score ).width( "1%" ).animate({
			width: relativeWidth + "%"
		}, 2000, function() {
			$( ".duration", this ).show();
			$( ".duration-unit", this ).show();
		});
		$( ".duration", bar ).hide();
		$( ".duration-unit", bar ).hide();
		$( "#popular-athlete-bars" ).append( bar );
	}
}

function getQuantity( score ) {
	if ( score && score > 0 ) {
		//score = score * 15;
		var minutes = score / 60;
		var value = 0;
		var unit = "minuten";
		if ( minutes < 1 ) {
			value = score;
			unit = "seconden";
		} else {
			value = Math.round( score / 60 );
			if ( value == 1) {
				unit = "minuut";
			}
		}
		return { value : value, unit : unit };
	} else {
		return { value : 0, unit : "seconden" };
	}
}


/*
 * Initialize the suggester
 */
function initSuggester( athletes ) {
	// JQuery UI Autocomplete 
	var ac = $( "#sporter-search-field" ).autocomplete({
		source: function( request, response ) {
			var matcher = new RegExp( $.ui.autocomplete.escapeRegex( request.term ), "i" );
			response( $.grep( athletes, function( athlete ) {
				return matcher.test( athlete.name );
			}));
		},
		select: function( event, ui ) { }
	});
	// Custom rendering
	ac.autocomplete( "instance" )._renderItem = function( ul, athlete ) {
		var matcher = new RegExp( $.ui.autocomplete.escapeRegex( this.term ), "i" );
		var quantity = getQuantity( athlete.score );
		var element = $(
			"<div>" +
				"<div class=\"sporter-tile-70 pull-left\"><img src=\"./img/faces/"+athlete.id+".jpg\"></div>" +
				"<div class=\"sporter-name\">" +
					"<div class=\"name\">" + athlete.name.replace( matcher, "<em>$&</em>" ) + "</div>" +
					"<div class=\"sport\">" + athlete.sport + "</div>" +
					"<div class=\"duration\">" + quantity.value + " " + quantity.unit + "</div>" +
				"</div>" +
			"</div>"
		);
		return $( "<li>" )
			.attr( "data-value", athlete.id )
			.append( element )
			.appendTo( ul );
	};
	// Custom sizing
	ac.autocomplete( "instance" )._resizeMenu = function() {
		var ul = this.menu.element;
		ul.outerWidth( $( "#sporter-search-field" ).outerWidth() );
	};
}


/*
 * Show the athlete tabs
 */
function showAthletes( athletes ) {
	var nav = $( "<span>" );
	for ( var i = 0; i <= 25; i++ ) {
		var character = String.fromCharCode( i + 65 );
		var filteredAthletes = athleteNamesStartingWith( athletes, character );
		// Navigation
		if ( filteredAthletes.length == 0 ) {
			nav.append( "<span>" + character + "</span>" );
		} else {
			nav.append( "<a data-toggle=\"tab\" role=\"tab\" href=\"#aznav-" + character + "\">" + character + "</a>" );
		}
		if ( i < 25 ) {
			nav.append(" | " );
		}
		// Tab pane
		var tab = $( "<div id=\"aznav-" + character + "\" class=\"tab-pane" + ( i == 0 ? " active" : "") + "\" role=\"tabpanel\"> </div>" );
		tab.append(
			"<div class=\"row\">" +
				"<div class=\"col-sm-12\">" +
					"<h3>" + character + "</h3>" +
				"</div>" +
			"</div>"
		);
		var tabPane = $( "<div class=\"row\">" );
		$.each( filteredAthletes, function( ix, athlete ) {
			var quantity = getQuantity( athlete.score );
			var tile = $(
				"<div class=\"sporter-tile col-xs-12 col-sm-6 col-md-4\">" +
					"<div class=\"sporter-tile-70 pull-left\"><img src=\"./img/faces/"+athlete.id+".jpg\"></div>" +
					"<div class=\"sporter-name\">" +
						"<div class=\"name\">" + athlete.name + "</div>" +
						"<div class=\"sport\">" + athlete.sport + "</div>" +
						"<div class=\"duration\">" + quantity.value + " " + quantity.unit + "</div>" +
					"</div>" +
				"</div>"	
			);
			tabPane.append( tile );
		});
		tab.append( tabPane );
		$( "#sporter-tabs" ).append( tab );
	}
	$( "#sporter-tabs-nav" ).append( nav );
}

function athleteNamesStartingWith( athletes, start ) {
	var matcher = new RegExp("^" + start, "i" );
	return $.grep( athletes, function( athlete ) {
		return matcher.test( athlete.name );
	});
}

function displayDate( epoch ) {
	var date = new Date( epoch );
	return date.toLocal('dd-MM-yyyy');
}

function displayRelativeTime(millis) {
	var LZ = function( x ) {
		return ( x<0 || x>9 ? "" : "0" ) + x;
	};
	var secs = millis / 1000;
	var result = LZ( Math.floor( secs % 60 ) );
	if ( secs >= 60 ) {
		result = LZ( Math.floor( secs / 60 ) % 60 ) + ":" + result;
	} else {
		result = "00:" + result;
	}
	if ( secs >= 3600 ) {
		result = Math.floor( secs / 3600 ) + ":" + result;
	}
	return result;
}
